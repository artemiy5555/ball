import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

class Ball extends Circle {

    double dx = -1, dy = -1;
    double mass;

    Ball(double x, double y, double radius, Color color) {
        super(x, y, radius);
        this.mass=radius;
        setFill(color);
        dx=rnd(-1,1);
        dy=rnd(-1,1);
        if(dx ==0&&dy==0){
            dx=-1;
            dy=-1;
        }
    }

    static double rnd(int min, int max) {
        max -= min;
        double x = (Math.random() * ++max) + min;
        return x;
    }

}

